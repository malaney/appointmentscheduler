<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

$loader->add('lessc', __DIR__.'/../vendor/lessphp');

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));


return $loader;
