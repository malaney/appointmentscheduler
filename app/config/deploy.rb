set :application, "Village Babies Appointment Scheduler"
set :domain,      "mail"
set :deploy_to,   "/virtuals/villagebabies.com/appsched"
set :app_path,    "app"

set :stages,        %w(production staging dev)
set :default_stage, "production"
set :stage_dir,     "app/config"
require 'capistrano/ext/multistage'

set :scm,         :git
set :repository,  "git@bitbucket.org:malaney/appointmentscheduler.git"
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, or `none`

set :model_manager, "doctrine"
# Or: `propel`

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain                         # This may be the same as your `Web` server
role :db,         domain, :primary => true       # This is where Symfony2 migrations will run

set  :keep_releases,  3

# Be more verbose by uncommenting the following line
# logger.level = Logger::MAX_LEVEL

set  :use_sudo,       false

set :use_composer, true
set :composer_options,      "--verbose --prefer-dist"
# set :update_vendors, true

set :writable_dirs,     ["app/cache", "app/logs"]
set :user, "mhill"
set :webserver_user, "daemon"
set :permission_method, :acl
set :use_set_permissions, true
set :dump_assetic_assets, true
set :shared_children, [app_path + "/logs", web_path + "/uploads"]
set :php_bin, "/usr/local/bin/php"

# Be more verbose by uncommenting the following line
# logger.level = Logger::MAX_LEVEL
#
namespace :deploy do
    desc "Symlink Bootstrap"
    task :symlink_bootstrap do
        run "#{try_sudo} sh -c 'cd #{latest_release} && #{php_bin} #{symfony_console} mopa:bootstrap:symlink:less'"
    end
end

after "deploy:symlink_bootstrap" do
    puts "--> Bootstrap Symlinked!".green
end

namespace :symfony do
  desc "Copy vendors from previous release"
  task :copy_vendors, :except => { :no_release => true } do
    if Capistrano::CLI.ui.agree("Do you want to copy last release vendor dir then do composer install ?: (y/N)")
      capifony_pretty_print "--> Copying vendors from previous release"

      run "cp -a #{previous_release}/vendor #{latest_release}/"
      capifony_puts_ok
    end
  end

  desc "Hard clear cache"
  task :hard_clear_cache, :except => { :no_release => true } do
    capifony_pretty_print "--> Hard clearing the production cache"
    run "#{try_sudo} sh -c 'cd #{latest_release} && rm -rf app/cache/prod/*'"
    run "#{try_sudo} sh -c 'cd #{latest_release} && rm -rf app/logs/*'"
    capifony_puts_ok
  end
end

before 'symfony:cache:warmup', 'symfony:hard_clear_cache'
# before 'symfony:cache:warmup', 'symfony:hard_clear_cache'
before 'symfony:bootstrap:build', 'symfony:hard_clear_cache'

# Symfony2 2.1
before "symfony:vendors:install", "symfony:copy_vendors"
# before 'symfony:composer:update', 'symfony:copy_vendors'



# after "deploy:finalize_update", "deploy:symlink_bootstrap"
before 'symfony:assetic:dump', 'deploy:symlink_bootstrap'
