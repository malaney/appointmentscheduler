<?php
namespace Obverse\AppSchedulerBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class ScheduleCategoryAdmin extends Admin
{
    protected $translationDomain = 'ObverseAppSchedulerBundle';
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('color', 'obverse_schedule_color_type')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('color')
        ;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        //  $errorElement
        //      ->with('name')
        //          ->assertMaxLength(array('limit' => 100))
        //      ->end()
        //  ;
    }
}
