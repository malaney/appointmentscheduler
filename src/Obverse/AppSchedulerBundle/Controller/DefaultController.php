<?php
namespace Obverse\AppSchedulerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Obverse\AppSchedulerBundle\Entity\ScheduleEntity;
use Obverse\AppSchedulerBundle\Entity\ScheduleInstance;
use Obverse\AppSchedulerBundle\Form\Type\RepeatOptionsType;
use Obverse\AppSchedulerBundle\Form\Type\AddAppointmentType;
use Obverse\AppSchedulerBundle\Form\Model\AddAppointment;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * Default controller.
 *
 * @Route("/schedule")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/test")
     * @Template()
     */
    public function testAction()
    {
        $client = new \Google_Client();
        $client->setApplicationName('AppScheduler');
        $client->setClientId('1068460001904.apps.googleusercontent.com');
        $client->setClientSecret('aupMxQ4MW-e1sPWpvCLL_4ba');
        $client->setRedirectUri('http://localhost');
        $client->setDeveloperKey('AIzaSyC3trSC2xKMNwhfgTcCABxUu8_nDn5UPl0');
        $cal = new \Google_Service_Calendar($client);

        // $service = new Google_AnalyticsService($client);


        // $cal = new Google_CalendarService($client);
        if (isset($_GET['logout'])) {
          unset($_SESSION['token']);
        }

        if (isset($_GET['code'])) {
          $client->authenticate($_GET['code']);
          $_SESSION['token'] = $client->getAccessToken();
          header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
        }

        if (isset($_SESSION['token'])) {
          $client->setAccessToken($_SESSION['token']);
        }

        if ($client->getAccessToken()) {
          $calList = $cal->calendarList->listCalendarList();
          print "<h1>Calendar List</h1><pre>" . print_r($calList, true) . "</pre>";


        $_SESSION['token'] = $client->getAccessToken();
        } else {
            $client->addScope('calendar');
          $authUrl = $client->createAuthUrl();
          print "<a class='login' href='$authUrl'>Connect Me!</a>";
        }
                return array('name' => 'malaney');
    }

    /**
     * @Route("/", options={"expose"=true})
     * @Template()
     */
    public function indexAction()
    {
        return array('name' => 'foo');
    }

    /**
     * delete instance.
     *
     * @Route("/delete", options={"expose"=true})
     * @PreAuthorize("hasRole('ROLE_ADMIN')")
     * @Template("ObverseAppSchedulerBundle:Default:drag.html.twig")
     */
    public function deleteAction()
    {
        // fetch instance id for delete.
        $request = $this->get('request');
        $formVal = $request->get('obverse_schedule_add_appointment');
        $id = $formVal['id'];

        // manager object
        $manager = $this->getDoctrine()->getManager();

        // make query into repository for check instance exist or not into database.
        $instance = $manager->getRepository('ObverseAppSchedulerBundle:ScheduleInstance')->find($id);

        // if instance not found than given error and redirected.
        if (!$instance) {
            // set flash message.
            $this->get('session')->getFlashBag()->add('error', 'no instance found!');
            return $this->redirect($this->generateUrl('obverse_appscheduler_default_index'));
        }

        // remove instance events.
        $em = $this->getDoctrine()->getManager();
        $em->remove($instance);
        $em->flush();
        return array('response' => '1');
    }

    /**
     * @Route("/add", options={"expose"=true})
     * @Template()
     */
    public function addAction()
    {
        $request = $this->getRequest();
        $date = $request->query->get('date');
        // create schedule entity object.
        $slot = new ScheduleEntity();
        // if date found than set default value.
        if ($date) {
            $startDate = new \DateTime($date);
            $endDate = clone $startDate;
            $endDate->add(new \DateInterval('PT30M'));
            $slot->setTitle('Available for Tour');
            $slot->setStartAt($startDate);
            $slot->setEndAt($endDate);
            $em = $this->getDoctrine()->getManager();
            $locationRepo = $em->getRepository('Obverse\AppSchedulerBundle\Entity\ScheduleLocation');
            $categoryRepo = $em->getRepository('Obverse\AppSchedulerBundle\Entity\ScheduleCategory');
            $slot->setLocation($locationRepo->find(1));
            $slot->setCategory($categoryRepo->find(1));
        }
        // create form builder
        $fb = $this->createFormBuilder($slot);

        // if add than call xmlhttprequest condition and else for edit.
        if ($request->isXmlHttpRequest()) {
            $isXHR = true;
            $fb->add('location', 'entity_hidden', array('my_class' => 'Obverse\AppSchedulerBundle\Entity\ScheduleLocation'));
            $fb->add('category', 'entity_hidden', array('my_class' => 'Obverse\AppSchedulerBundle\Entity\ScheduleCategory'));
            $fb->add('title');
            $fb->add('capacity');

            $fb->add('startAt', 'datetime', array('label' => false, 'date_widget' => 'single_text', 'time_widget' => 'single_text', 'attr' => array('style' => 'display: none')));
            $fb->add('endAt', 'datetime', array('label' => false, 'date_widget' => 'single_text', 'time_widget' => 'single_text', 'attr' => array('style' => 'display: none')));
        } else {
            $isXHR = false;
            $fb->add('location', 'entity_hidden', array(
                'my_class' => 'Obverse\AppSchedulerBundle\Entity\ScheduleLocation',
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'horizontal_label_class' => 'col-lg-3'
            ));
            $fb->add('category', 'entity_hidden', array(
                'my_class' => 'Obverse\AppSchedulerBundle\Entity\ScheduleCategory',
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'horizontal_label_class' => 'col-lg-3'
            ));
            $fb->add('title', null, array(
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'horizontal_label_class' => 'col-lg-3'
            ));
            $fb->add('capacity', null, array(
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'horizontal_label_class' => 'col-lg-3'
            ));

            $fb->add('startAt', 'datetime', array(
                'date_widget' => 'single_text', 
                'time_widget' => 'single_text',
                'horizontal_input_wrapper_class' => 'col-lg-2',
                'horizontal_label_class' => 'col-lg-3'
            ));
            $fb->add('endAt', 'datetime', array(
                'date_widget' => 'single_text', 
                'time_widget' => 'single_text',
                'horizontal_input_wrapper_class' => 'col-lg-2',
                'horizontal_label_class' => 'col-lg-3'
            ));
            $fb->add('rrule', 'hidden', array(
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'horizontal_label_class' => 'col-lg-3'
            ));
            $fb->add('options', 'obverse_schedule_options', array(
                'label' => 'Repeat Options', 
                'required' => false,
                'render_optional_text' => false,
                'widget_type' => 'inline',
                'horizontal_input_wrapper_class' => 'col-lg-4',
                'horizontal_label_class' => 'col-lg-3'
            ));
        }
        // get form object.
        $form = $fb->getForm();

        // if form post than bind and save values.
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                // save booking slot
                $em = $this->getDoctrine()->getManager();
                $em->persist($slot);
                $em->flush();

                // set flash message.
                $this->get('session')->getFlashBag()->add('success', 'Schedule Entity Added');
                return $this->redirect($this->generateUrl('obverse_appscheduler_default_index'));
            }
        }
        if ($isXHR) {
            return $this->render('ObverseAppSchedulerBundle:Default:addXHR.html.twig', array(
                'form' => $form->createView()
            ));
        } else {
            return $this->render('ObverseAppSchedulerBundle:Default:add.html.twig', array(
                'form' => $form->createView()
            ));
        }

        // return array('isXHR' => $isXHR, 'form' => $form->createView());
    }

    /**
     * @Route("/add/appointment", name="obverse_appscheduler_default_add_appointment", options={"expose"=true})
     * @Template()
     */
    public function addAppointmentAction()
    {
        $request = $this->get('request');
        $formVal = $request->get('obverse_schedule_add_appointment');
        $id = (!$request->get('id')) ? $formVal['id'] : $request->get('id') ;

        $manager = $this->getDoctrine()->getManager();

        // make query into repository for check instance exist or not.
        $instance = $manager->getRepository('ObverseAppSchedulerBundle:ScheduleInstance')->find($id);

        // if instance not found than given error and redirected.
        if (!$instance) {
            // set flash message.
            $this->get('session')->getFlashBag()->add('error', 'no instance found!');
            return $this->redirect($this->generateUrl('obverse_appscheduler_default_index'));
        }

        $response = new Response();
        // checked server side if pending/booked instance than no other user can booked.
        if ($instance->getStatus() == 'pending' || $instance->getStatus() == 'booked') {
            // set error message.
            $output = array('success' => false, 'message' => 'This slot is booked, so please choose another.');
            $response->headers->set('Content-Type', 'application/json');
            // set json content
            $response->setContent(json_encode($output));

            // return json response.
            return $response;
        }

        // call add appointment forms.
        $appointmentForm = $this->createForm(
           new AddAppointmentType($instance->getId()), new AddAppointment()
        );

        // if form post than save value.
        if ($request->isMethod('POST')) {
            $appointmentForm->bind($request);

            if ($appointmentForm->isValid()) {
                // make guest arrays with email, mob and notes.
                $formData = $appointmentForm->getData();
                $guests = array('email' => $formData->getEmail(), 'phone' => $formData->getMobilePhone(), 'notes' => $formData->getNotes());
                // call entity manager.
                $em = $this->getDoctrine()->getManager();
                // set status of instancce and guest values.
                $instance->setStatus('pending');
                $instance->setGuests($guests);
                // persist records.
                $em->persist($instance);
                $em->flush();

                $bodyContent = $this
                    ->renderView(
                        'ObverseAppSchedulerBundle:Default:emailMessageNotification.html.twig',
                        array('instanceId' => $instance->getId(),));

                // $email = \Swift_Message::newInstance()
                //     ->setSubject("Appointment is pending")
                //     //->setFrom($sender->getEmail())
                //     ->setFrom(array('info@appointmentschedule.com' => 'Appointment Schedule'))
                //     ->setTo($formData->getEmail())->setContentType("text/html")
                //     ->setBody($bodyContent);

                // $this->get('mailer')->send($email);

                // set success message and redirected to calendar event list.
                $output = array('success' => true, 'id' => $instance->getId());
                $response->headers->set('Content-Type', 'application/json');
                // set json content
                $response->setContent(json_encode($output));

                // return json response.
                return $response;
            }
        }

        // return to templates
        return array('instance' => $instance, 'form' => $appointmentForm->createView());
    }

    /**
     * call this method when click on confirmation link from email.
     *
     * @Route("/mail/confirm/{id}", name="obverse_appscheduler_default_mail_confirm", options={"expose"=true})
     * @Template()
     */
    public function mailConfirmAction($id)
    {
        // manager object
        $manager = $this->getDoctrine()->getManager();

        // make query into repository for check instance exist or not.
        $instance = $manager->getRepository('ObverseAppSchedulerBundle:ScheduleInstance')->find($id);

        // if instance not found than given error and redirected.
        if (!$instance || $instance->getStatus() != 'pending') {
            // set flash message.
            $this->get('session')->getFlashBag()->add('error', 'no instance found!');
            return $this->redirect($this->generateUrl('obverse_appscheduler_default_index'));
        }

        // doctrine manager.
        $em = $this->getDoctrine()->getManager();
        // set status of instancce and guest values.
        $instance->setStatus('booked');
        // persist records.
        $em->persist($instance);
        $em->flush();
        return array('instance' => $instance);
    }

    /**
     * drag
     * Process drag event on scheduleInstance (add to both start and end of event)
     * @Route("/drag/{id}/{dayDelta}/{minuteDelta}/{isAllDay}", options={"expose"=true})
     * @PreAuthorize("hasRole('ROLE_ADMIN')")
     * @Template()
     */
    public function dragAction(ScheduleInstance $scheduleInstance, $dayDelta, $minuteDelta, $isAllDay)
    {
        $interval = sprintf('P%dDT%dM', $dayDelta, $minuteDelta);
        $start = $scheduleInstance->getStart();
        $end = $scheduleInstance->getEnd();
        $finalStart = $this->addInterval($start, $dayDelta, 'day');
        $finalStart = $this->addInterval($finalStart, $minuteDelta, 'minute');
        $finalEnd = $this->addInterval($end, $dayDelta, 'day');
        $finalEnd = $this->addInterval($finalEnd, $minuteDelta, 'minute');

        $scheduleInstance->setStart($finalStart);
        $scheduleInstance->setEnd($finalEnd);
        $scheduleInstance->setIsAllDay($isAllDay == 1);

        $em = $this->getDoctrine()->getManager();
        $em->persist($scheduleInstance);
        $em->flush();
        return array('response' => '1');
    }

    /**
     * resize
     * Process resize event(add to end of event)
     * @Route("/resize/{id}/{dayDelta}/{minuteDelta}", options={"expose"=true})
     * @Template("ObverseAppSchedulerBundle:Default:drag.html.twig")
     * @PreAuthorize("hasRole('ROLE_ADMIN')")
     * @Method("POST")
     */
    public function resizeAction(ScheduleInstance $scheduleInstance, $dayDelta, $minuteDelta)
    {
        $end = $scheduleInstance->getEnd();
        $finalEnd = $this->addInterval($end, $dayDelta, 'day');
        $finalEnd = $this->addInterval($finalEnd, $minuteDelta, 'minute');
        // $finalEnd = $finalEnd->add(new \DateInterval($interval));
        $scheduleInstance->setEnd($finalEnd);
        $em = $this->getDoctrine()->getManager();
        $em->persist($scheduleInstance);
        $em->flush();
        return array('response' => '1');
    }

    /**
     * @Route("/edit/{id}")
     * @Template()
     */
    public function editAction($id)
    {

    }

    /**
     * @Route("/repeat")
     * @Template()
     */
    public function repeatAction()
    {
        $form = $this->get('form.factory')->create(new RepeatOptionsType());
        return array('form' => $form->createView());
    }

    /**
     * @Route("/fullcalendar" , options={"expose"=true})
     *
     */
    public function fullcalendarAction(Request $request)
    {
        $start = $request->get('start') ? $request->get('start') : time();
        $end = $request->get('end') ? $request->get('end') : strtotime("+1 month");

        // set datetime object for request start and end date.
        $startDatetime = new \DateTime();
        $startDatetime->setTimestamp($start);

        $endDatetime = new \DateTime();
        $endDatetime->setTimestamp($end);

        // manager object
        $manager = $this->getDoctrine()->getManager();

        $sc = $this->get('security.context');
        $isAdmin = false;
        foreach ($sc->getToken()->getRoles() as $role) {
            if (($role->getRole() == 'ROLE_ADMIN')) {
                $isAdmin = true;
            }
        }

        // make query into repository for fetch instance with relation to entity.
        $entity = $manager->getRepository('ObverseAppSchedulerBundle:ScheduleInstance')->getInstanceByDate($startDatetime, $endDatetime, $isAdmin);

        // create an instance array for display into calendar
        $entityArr = array();
        if (count($entity) > 0) {
            foreach ($entity as $entites) {
               foreach ($entites->getScheduleInstances() as $instance) {
                  $status = $instance->getStatus();
                  switch ($status) {
                    case 'pending':
                        $color = '#cccc00';
                    break;
                    case 'booked':
                        $color = '#cc0000';
                    break;
                    default:
                        $color = '#0000cc';
                    break;
                  }
                  $entityArr[] = array(
                       'title' => $instance->getTitle(),
                       'start' => $instance->getStart()->format('Y-m-d h:i:s'),
                       'end' => $instance->getEnd()->format('Y-m-d h:i:s'),
                       'id' => $instance->getId(),
                       'color' => $color,
                       'status' => $instance->getStatus(),
                       'allDay' => $instance->getIsAllDay()
                   );
               }
            }
        }

        // create response object.
        $response = new Response();
        // create a JSON-response
        $response->headers->set('Content-Type', 'application/json');
        // set json content
        $response->setContent(json_encode($entityArr));
        // return json response.
        return $response;
    }

    /**
     * addInterval
     * Adds a set amount of time to date object
     *
     * @param DateTime $origDateTime  // DateTime obj to be acted upon
     * @param integer $amount  // amount to add (can be pos or neg)
     * @param string $unit  // "day" or "minute"
     * @access public
     * @return DateTime
     */
    public function addInterval(\DateTime $origDateTime, $amount, $unit)
    {
        $finalDateTime = clone $origDateTime;
        if ($amount != 0) {
            $method = ($amount > 0) ? 'add' : 'sub';
            $amount = abs($amount);
            switch ($unit) {
                case 'day':
                    $interval = sprintf('P%dD', $amount);
                break;

                case 'minute':
                    $interval = sprintf('PT%dM', $amount);
                break;

                default:
                    throw new \Exception("Invalid unit $unit");
                break;
            }
            $finalDateTime->{$method}(new \DateInterval($interval));
        }
        return $finalDateTime;
    }

}
