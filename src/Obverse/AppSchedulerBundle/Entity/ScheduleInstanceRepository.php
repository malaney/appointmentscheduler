<?php

namespace Obverse\AppSchedulerBundle\Entity;

use Doctrine\ORM\Query\Expr;

use Doctrine\ORM\EntityRepository;
use DateTime;
use Obverse\AppSchedulerBundle\Entity\ScheduleEntity;
use Obverse\AppSchedulerBundle\Entity\ScheduleInstance;

/**
 * ScheduleInstanceRepository
 *
 */
class ScheduleInstanceRepository extends EntityRepository
{

    /**
     * findAllInstanceByDate
     *
     * @param Datetime $startDate
     * @param Datetime $endDate
     *
     * @return ArrayCollection
     *
     */
    public function getInstanceByDate(DateTime $startDate, DateTime $endDate, $isAdmin=false)
    {
        $qb = $this
         ->getEntityManager()
         ->createQueryBuilder();
        $qb
         ->select('se, si')
         ->from('ObverseAppSchedulerBundle:ScheduleEntity', 'se')
         ->leftJoin('se.scheduleInstances', 'si')
         ->where('si.start > :startAt')
         ->andWhere('si.end < :endAt');
if (!$isAdmin) {
        $qb
         // ->andWhere('si.status != :open')
         ->andWhere('si.start >= :today');
        ;
}
        $qb
         ->setParameter('startAt', $startDate->format('Y-m-d H:i:s'))
         ->setParameter('endAt', $endDate->format('Y-m-d H:i:s'));
if (!$isAdmin) {
        $today = new \DateTime();
        $qb
        // ->setParameter(':open', 'open')
        ->setParameter(':today', $today->format('Y-m-d'));
        ;
}
        return $qb
         ->getQuery()
         ->getResult();
    }

}
