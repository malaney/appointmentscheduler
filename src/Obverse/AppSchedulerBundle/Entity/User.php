<?php

namespace Obverse\AppSchedulerBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Table(name="user")
 * @ORM\Entity
 *
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email",
 *          column=@ORM\Column(type="string", name="email", length=255, unique=false, nullable=true))
 * })
 * @UniqueEntity(fields="email", message="Email is already in use")
 *
 */
class User extends BaseUser
{
  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @var \string
   *
   * @ORM\Column(name="address", type="string", nullable=true)
   */
  protected $address;

  /**
   * @var \string
   *
   * @ORM\Column(name="city", type="string", nullable=true)
   */
  protected $city;

  /**
   * @var \string
   *
   * @ORM\Column(name="state", type="string", nullable=true)
   */
  protected $state;

  /**
   * @var \integer
   *
   * @ORM\Column(name="zip", type="integer", nullable=true)
   */
  protected $zip;

  /**
   * @var \string
   *
   * @ORM\Column(name="mobile_phone", type="string", nullable=true)
   */
  protected $mobile_phone;

  /**
   * Used only for serializing
   */
  protected $first_name;

  /**
   * Used only for serializing
   */
  protected $last_name;

  /**
   * Constructor
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set address
   *
   * @param string $address
   * @return User
   */
  public function setAddress($address)
  {
    $this->address = $address;

    return $this;
  }

  /**
   * Get address
   *
   * @return string
   */
  public function getAddress()
  {
    return $this->address;
  }

  /**
   * Set city
   *
   * @param string $city
   * @return User
   */
  public function setCity($city)
  {
    $this->city = $city;

    return $this;
  }

  /**
   * Get city
   *
   * @return string
   */
  public function getCity()
  {
    return $this->city;
  }

  /**
   * Set state
   *
   * @param string $state
   * @return User
   */
  public function setState($state)
  {

    $this->state = $state;
    return $this;
  }

  /**
   * Get state
   *
   * @return string
   */
  public function getState()
  {
    return $this->state;
  }

  /**
   * Set zip
   *
   * @param integer $zip
   * @return User
   */
  public function setZip($zip)
  {
    $this->zip = $zip;

    return $this;
  }

  /**
   * Get zip
   *
   * @return integer
   */
  public function getZip()
  {
    return $this->zip;
  }

  /**
   * Set mobile_phone
   *
   * @param string $mobilePhone
   * @return User
   */
  public function setMobilePhone($mobilePhone)
  {
    $this->mobile_phone = $mobilePhone;

    return $this;
  }

  /**
   * Get mobile_phone
   *
   * @return string
   */
  public function getMobilePhone()
  {
    return $this->mobile_phone;
  }

  /**
   * Set firstName
   *
   * @param string $firstName
   * @return User
   */
  public function setFirstName($firstName)
  {
    $this->firstname = $firstName;

    return $this;
  }

  /**
   * Get firstName
   *
   * @return string
   */
  public function getFirstName()
  {
    return $this->firstname;
  }

  /**
   * Set lastName
   *
   * @param string $lastName
   * @return User
   */
  public function setLastName($lastName)
  {
    $this->lastname = $lastName;

    return $this;
  }

  /**
   * Get lastName
   *
   * @return string
   */
  public function getLastName()
  {
    return $this->lastname;
  }

  /**
   * Return string of Fullname
   *
   * @return string
   */
  public function getFullName() {
    return $this->getFirstName(). ' '. $this->getLastName();
  }

}