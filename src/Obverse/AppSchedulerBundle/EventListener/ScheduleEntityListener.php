<?php
namespace Obverse\AppSchedulerBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class ScheduleEntityListener
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (get_class($entity) == 'Obverse\AppSchedulerBundle\Entity\ScheduleEntity') {
            $processor = $this->container->get('obverse_schedule.processor.schedule_instances');
            $processor->processEntity($entity);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        return $this->postPersist($args);
    }
}
