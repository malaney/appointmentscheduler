<?php

namespace Obverse\AppSchedulerBundle\Form\Model;

use \Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

/**
 * Form model class
 *
 * @author Sandip Limbachiya
 */
class AddAppointment
{

  /**
   * The instance id
   */
  protected $id;

  /**
   * email
   *
   * @var string email
   *
   * @Assert\NotBlank(message="Please enter your email address")
   * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
   */
  protected $email;

  /**
   * mobile_phone
   *
   * @AssertPhoneNumber(defaultRegion="US")
   * @var string mobile_phone
   */
  protected $mobile_phone;

  /**
   * The notes
   *
   */
  protected $notes;

  public function __construct()
  {
  }

  /**
   * Sets id
   *
   * @param string $instaceid
   */
  public function setId($instaceid)
  {
    $this->id = $instaceid;
  }


  /**
   * Return the instance id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Return the mobile phone
   *
   * @return string
   */
  public function getMobilePhone()
  {
    return $this->mobile_phone;
  }

  /**
   * Sets the mobile phone
   *
   * @param string $mobilePhone
   */
  public function setMobilePhone($mobilePhone)
  {
    $this->mobile_phone = $mobilePhone;
  }

  /**
   * Return the email
   *
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Sets the email
   *
   * @param string $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * Return the notes
   *
   * @return string
   */
  public function getNotes()
  {
    return $this->notes;
  }

  /**
   * Sets the notes
   *
   * @param string $notes
   */
  public function setNotes($notes)
  {
    $this->notes = $notes;
  }

}
