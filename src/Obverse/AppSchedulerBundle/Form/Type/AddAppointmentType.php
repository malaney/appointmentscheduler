<?php

namespace Obverse\AppSchedulerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use libphonenumber\PhoneNumberFormat;

/**
 * Add appointment form.
 */
class AddAppointmentType extends AbstractType
{
    private $instanceId;

    public function __construct($instanceId){
        $this->instanceId = $instanceId;
    }

    /**
     * buildform
     *
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email', array(
                        'label' => 'Email Address',
                        'horizontal_input_wrapper_class' => 'col-lg-6', 
                        'horizontal_label_class' => 'col-lg-3',
                        'required' => true)
        );

        $builder->add('mobile_phone', 'tel', array(
                        'label' => 'Phone', 
                        'default_region' => 'US',
                        'format' => PhoneNumberFormat::NATIONAL,
                        'required' => true, 
                        'horizontal_input_wrapper_class' => 'col-lg-6', 
                        'horizontal_label_class' => 'col-lg-3',
                        )
            );

        $builder->add('notes', 'textarea', array(
                        'label' => 'Notes',
                        'horizontal_input_wrapper_class' => 'col-lg-6', 
                        'horizontal_label_class' => 'col-lg-3',
                        'required' => false)
        );

        $builder->add('id', 'hidden', array('data' => $this->instanceId));
    }

    /**
     * get name of form.
     *
     * @return string
     */
    public function getName()
    {
        return 'obverse_schedule_add_appointment';
    }
}
