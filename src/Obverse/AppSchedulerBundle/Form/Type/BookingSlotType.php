<?php
namespace Obverse\AppSchedulerBundle\Form\Type;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookingSlotType extends AbstractType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    public function getName()
    {
        return 'obv_booking_slot';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('quantity');
        $builder->add('startDate', 'datetime', array('label' => 'Start Date', 'widget' => 'single_text'));
        $builder->add('endDate', 'datetime', array('label' => 'End Date', 'widget' => 'single_text'));
    }
}
