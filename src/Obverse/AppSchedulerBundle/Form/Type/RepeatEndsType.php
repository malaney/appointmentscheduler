<?php
namespace Obverse\AppSchedulerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;

class RepeatEndsType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'compound' => true,
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('ends_type', 'choice', array(
            'label' => false,
            'choices' => array(
                'never' => 'Never',
                'after' => 'After',
                'until' => 'Until'
            ),
            'expanded' => true,
            'multiple' => false,
            'horizontal_input_wrapper_class' => 'col-lg-6', 
            'horizontal_label_class' => 'col-lg-3',
            'data' => 'never' // pre-selected option
        ));
        $builder->add('num_occurrences', 'text', array(
            'attr' => array('class' => 'input-mini'),
            'horizontal_input_wrapper_class' => 'boop', 
            'horizontal_label_class' => 'col-lg-3',
            'required' => false
        ));
        $builder->add('until_date', 'date', array(
            'widget' => 'single_text',
            'horizontal_input_wrapper_class' => 'col-lg-4', 
            'horizontal_label_class' => 'col-lg-3',
            'required' => false
        ));
    }

    public function getParent()
    {
        return 'form';
    }

    public function getName()
    {
        return 'obverse_schedule_repeat_ends';
    }
}
