<?php
namespace Obverse\AppSchedulerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Obverse\AppSchedulerBundle\Form\Type\RepeatEndsType;

class RepeatOptionsType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'compound' => true
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('frequency', 'choice', array(
            'choices' => array(
                '3' => 'Daily',
                '2' => 'Weekly',
                '1' => 'Monthly',
                '0' => 'Yearly'
            ),
            'horizontal_input_wrapper_class' => 'col-lg-4', 
            'horizontal_label_class' => 'col-lg-3',
            'attr' => array('class' => 'input-medium')
        ));
        $builder->add('rinterval', 'choice', array(
            'choices' => array_combine(range(1,30), range(1,30)),
            'label' => 'Repeat every',
            'horizontal_input_wrapper_class' => 'col-lg-4',
            'horizontal_label_class' => 'col-lg-3',
            'attr' => array('class' => 'input-medium'),
            'widget_prefix' => '<span id="repeat_interval">Prefix Text</span>'
        ));
        $builder->add('repeat_weekday', 'choice', array(
            'label' => 'Repeat on',
            'label_attr' => array('class' => 'inline'),
            'choices' => array(
                '6' => 'Su',
                '0' => 'Mo',
                '1' => 'Tu',
                '2' => 'We',
                '3' => 'Th',
                '4' => 'Fr',
                '5' => 'Sa'
            ),
            'expanded' => true,
            'horizontal_input_wrapper_class' => 'col-lg-8', 
            'horizontal_label_class' => 'col-lg-3',
            'widget_type' => 'inline',
            'widget_form_group_attr' => array('class' => 'repeat_qualifier'),
            'multiple' => true,
        ));
        $builder->add('repeat_monthday', 'choice', array(
            'label' => 'Repeat on',
            'label_attr' => array('class' => 'inline'),
            'choices' => array(
                'month_day' => 'day of the month',
                'week_day' => 'day of the week',
            ),
            'expanded' => true,
            'multiple' => false,
            'widget_type' => 'inline',
            'render_optional_text' => false,
            'horizontal_input_wrapper_class' => 'col-lg-8', 
            'horizontal_label_class' => 'col-lg-3',
            'required' => false,
            'widget_form_group_attr' => array('class' => 'repeat_qualifier')
        ));

        $builder->add('start', 'date', array(
            'widget' => 'single_text',
            'label' => 'Starts on',
            'horizontal_input_wrapper_class' => 'col-lg-4', 
            'horizontal_label_class' => 'col-lg-3',
            'data' => new \DateTime(),
            'attr' => array('class' => 'input-medium')
        ));
        // $builder->add('ends', new RepeatEndsType(), array());
        $builder->add('ends_type', 'choice', array(
            'label' => 'Ends',
            'choices' => array(
                'never' => 'Never',
                'after' => 'After',
                'until' => 'On'
            ),
            'expanded' => true,
            'multiple' => false,
            'horizontal_input_wrapper_class' => 'col-lg-3', 
            'horizontal_label_class' => 'col-lg-3',
            'data' => 'never' // pre-selected option
        ));
        $builder->add('num_occurrences', 'text', array(
            'attr' => array('class' => 'input-mini'),
            'horizontal_input_wrapper_class' => 'col-lg-2',
            'horizontal_label_class' => 'col-lg-3',
            'render_optional_text' => false,
            'label' => ' ',
            'widget_prefix' => 'occurrences',
            'widget_form_group_attr' => array('class' => 'ends_qualifier'),
            'required' => false,
        ));
        $builder->add('until_date', 'date', array(
            'widget' => 'single_text',
            'label' => ' ',
            'render_optional_text' => false,
            'horizontal_input_wrapper_class' => 'col-lg-4', 
            'horizontal_label_class' => 'col-lg-3',
            'widget_form_group_attr' => array('class' => 'ends_qualifier'),
            'required' => false,
        ));
    }

    public function getParent()
    {
        return 'form';
    }

    public function getName()
    {
        return 'obverse_schedule_repeat_options';
    }
}
