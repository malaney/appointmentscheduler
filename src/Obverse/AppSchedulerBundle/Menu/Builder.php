<?php
namespace Obverse\AppSchedulerBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        // This will add the proper classes to your UL
        // Use push_right if you want your menu on the right
        $menu = $factory->createItem('root', array(
            'navbar' => true,
            'pull-right' => false,
        ));

        // Regular menu item, no change
        $menu->addChild('Village Babies', array('route' => 'sonata_admin_dashboard'));

        // Create a dropdown
        $orgDropdown = $menu->addChild('Schedule', array(
            'dropdown' => true,
            'caret' => true,
        ));

        $orgDropdown->addChild('View Schedule', array('route' => 'obverse_appscheduler_default_index'));

        return $menu;
    }

    public function rightSideMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
            'navbar' => true,
            'pull-right' => true,
            'push_right' => false,
        ));
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();

        if (is_object($user)) {
            $menu->addChild('Welcome ' . $user, array('route' => 'sonata_user_profile_show'));
            $menu->addChild('Logout', array('route' => 'fos_user_security_logout'));
        } else {
            $menu->addChild('Login', array('route' => 'fos_user_security_login'));
        }

        return $menu;
    }
}
