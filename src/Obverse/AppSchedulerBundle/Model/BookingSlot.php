<?php
namespace Obverse\AppSchedulerBundle\Model;

class BookingSlot
{
    private $quantity;
    private $startDate;
    private $endDate;
 
    /**
     * Get quantity.
     *
     * @return quantity.
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
 
    /**
     * Set quantity.
     *
     * @param quantity the value to set.
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }
 
    /**
     * Get startDate.
     *
     * @return startDate.
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
 
    /**
     * Set startDate.
     *
     * @param startDate the value to set.
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }
 
    /**
     * Get endDate.
     *
     * @return endDate.
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
 
    /**
     * Set endDate.
     *
     * @param endDate the value to set.
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }
}
